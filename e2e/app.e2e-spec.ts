import { OvidiuoPage } from './app.po';

describe('ovidiuo App', () => {
  let page: OvidiuoPage;

  beforeEach(() => {
    page = new OvidiuoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
