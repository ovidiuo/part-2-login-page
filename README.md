# Ovidiuo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.3.

## Install
  Required:

    'Angular Cli' installed

  Install Angular Cli :

     > npm install -g @angular/cli


##  Porject setup:

   > git clone git@bitbucket.org:ovidiuo/part-2-login-page.git

   > cd part-2-login-page

   > npm install

   > ng s 


   > http://localhost:4200/



## Technologies 
    > Angula Material
    > Bootstrap
    > Font awesome 

##  Key points:
    > 3. - Angular Material - used for login form and for Toast (Android Style)
    >    - Bootstrap - used for grid system 
    >    - Font awesome - used for icons 

    > 5. Angular-cli - was the best choice for my project, the other option "Angular advance seed" was too complex that was used (Electron,Native mobile app) and i did not need that for my project.
    > Angular-Cli is easy to use and  offers a lot of options for fast development.  

    > 7. - for css/js optimized i was use an angular-cli Command  Line  call "ng build --prod --env=prod" is minify and add a file hash for me.