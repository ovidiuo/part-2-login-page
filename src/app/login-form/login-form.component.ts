import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { ValidationService } from '../service/validation.service';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  userForm: any;

  constructor(public toast: MdSnackBar, private formBuilder:FormBuilder) {
    this.userForm = this.formBuilder.group({
      'email': ['', [Validators.required, ValidationService.emailValidator]],
      'password': ['', Validators.required],
    });

   }
  
   saveUser() {
     if(this.userForm.dirty && this.userForm.valid){
      this.openSnackBar()
      console.log("You have submit:", `Email: ${this.userForm.value.email} / Passowrd: ${this.userForm.value.password}`)
     }
   }
  openSnackBar(){
    this.toast.open('Login successful!','',{
      duration: 900
    });
  }

  ngOnInit() {
  }

}
