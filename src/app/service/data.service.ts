import { Injectable } from '@angular/core';


@Injectable()
export class DataService {
   constructor() { } 
  getData(){
       return  [
    { 
    "id":1,
    "title":"Geiranger",
    "image":"https://res.cloudinary.com/simpleview/image/upload/c_fill,f_auto,h_280,q_64,w_560/v1/clients/norway/unesco-geirangerfjord-skagefla-waterfall-2-1_6cc6a64a-a204-432e-8753-01ef2080f24e.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
    },
    { 
    "id":2,
    "title":"Norway",
    "image":"https://res.cloudinary.com/simpleview/image/upload/c_fill,f_auto,h_280,q_64,w_560/v1/clients/norway/98e8fb32_f11f_4db6_8ead_438dc36cea86_06384cd1-7890-4012-85d7-67567fc75f95.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
    
  },
    {
    "id":3,
    "title":"Bergen",
    "image":"https://res.cloudinary.com/simpleview/image/upload/c_fill,f_auto,h_575,q_64,w_1200/v1/clients/norway/bryggen_wharf_in_bergen_norway_2_1_cf2fbd46-e9f6-4ae0-a3b7-50a4655f8788.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
       
   },

    {
      "id":4,
    "title":"Plitvice",
    "image":"http://www.naturespy.org/wp-content/uploads/2014/09/20140829_154938-560x280.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"        
  },
   {
      "id":5,
    "title":"Stavkirke",
    "image":"https://res.cloudinary.com/simpleview/image/fetch/c_fill,h_280,q_64,w_560/https://media.newmindmedia.com/TellUs/image/%3Ffile%3D949F39C1F41AE0B47FB93DAB4EB7D127E8DB9506.jpg%26dh%3D800%26dw%3D1200",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
       
  },
   {
      "id":6,
    "title":"Ålesund",
    "image":"https://www.wallpaperink.co.uk/gallery/shutterstock/Climbing/Wall_Mural_On_Top_World_Man_Standing1187620741.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
       
  },
   {
    "id":7,
    "title":"Jurassic",
    "image":"http://www.scified.com/media-thumbs/jurassicpark4movie.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  },
   {
      "id":8,
    "title":"Lofoten",
    "image":"https://res.cloudinary.com/simpleview/image/fetch/c_fill,h_280,q_64,w_560/http://images.citybreak.com/image.aspx%3FImageId%3D3983920",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  },
  {
      "id":9,
    "title":"Ricoh",
    "image":"https://images.wondershare.com/filmora/article-images/ricoh-theta-s-image-quality.png",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  },
  {
      "id":10,
    "title":"Harstad",
    "image":"https://res.cloudinary.com/simpleview/image/upload/c_fill,f_auto,h_280,q_64,w_560/v1/clients/norway/northern-lights-tromso-norway_2-1_a8b03e36-f1cd-46be-939e-ebf6d70c41e2.jpg",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  },
  {
      "id":11,
    "title":"Geirangerfjord",
    "image":"https://res.cloudinary.com/simpleview/image/fetch/c_fill,h_280,q_64,w_560/https://media.newmindmedia.com/TellUs/image/%3Ffile%3D1819A0FB27ADFDDA9205D369E341C75704929E26.jpg%26dh%3D534%26dw%3D800",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  },
  {
      "id":12,
    "title":"Delhi",
    "image":"https://holidaymango.files.wordpress.com/2015/08/paradigling.jpg?w=640",
    "content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
        
  }
]
  }
  
}
