import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';

import { MainPageComponent } from '../main/main-page.component';

import { ErrorComponent } from '../error/error.component';

 
  const routes: Routes = [
    { path:"", component:MainPageComponent },
    { path:"**", component:ErrorComponent }
  ]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRouteModule { }
