import { Component, OnInit } from '@angular/core';
import { MdButtonModule } from '@angular/material';
import { DataService } from '../service/data.service';


@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css'],
  providers: [DataService]
})
export class MainPageComponent implements OnInit {

  contentData = [];
  public alesid;
  constructor(private data:DataService) { }


  ngOnInit() {
    this.contentData = this.data.getData()
    
  }

}